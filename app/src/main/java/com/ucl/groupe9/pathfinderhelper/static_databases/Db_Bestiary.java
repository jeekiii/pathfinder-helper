package com.ucl.groupe9.pathfinderhelper.static_databases;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.ucl.groupe9.pathfinderhelper.R;
import com.ucl.groupe9.pathfinderhelper.model.Monster;
import com.ucl.groupe9.pathfinderhelper.puppeteer.MobSQLiteHelper;

import java.util.ArrayList;

public class Db_Bestiary extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private ArrayList<Monster> tmp_originalData;
    private ListView mobList;
    private SearchView searchBar;
    private MobSQLiteHelper mDb = null;

    private Db_Bestiary_Adapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db_bestiary);

        //bestiaryList = (ListView) findViewById(R.id.listView);
        Thread t = new Thread(new Runnable() {
            public void run()
            {
                tmp_originalData = Monster.getMobs();
            }
        });
        t.start();


        //Let's store the names of all monsters
        searchBar = (SearchView) findViewById(R.id.bestiarySearch);
        mobList = (ListView) findViewById(R.id.listView);


        try {
            t.join();
            this.adapter = new Db_Bestiary_Adapter(this,tmp_originalData);
        } catch (InterruptedException e) {
            Log.w("DEBUG","Error on thread");
        }

       searchBar.setOnCloseListener(new SearchView.OnCloseListener(){
            @Override
            public boolean onClose() {
                adapter.getFilter().filter("");
                Log.w("DEBUG","CLOSE");
                return true;
            }
        });


        mobList.setTextFilterEnabled(true);
        setupSearchView();
        mobList.setAdapter(adapter);

    }
    private void setupSearchView() {
        searchBar.setIconifiedByDefault(false);
        searchBar.setOnQueryTextListener(this);
        searchBar.setSubmitButtonEnabled(false);
        searchBar.setQueryHint(getString(R.string.db_bestiary_hint));
    }

    public boolean onQueryTextChange(String newText) {
        if (newText.equals("")) {
            adapter.getFilter().filter("");
            Log.w("DEBUG","CLEAR");
        }
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        adapter.getFilter().filter(query);
        return true;
    }

    public void rowClickMobs(View v) {
        LinearLayout linearL = (LinearLayout) v;
        TextView tv = (TextView) linearL.getChildAt(0);
        String name = tv.getText().toString();
        //get description
        String description = "no description found";

        for (int i = 0; i < adapter.getFullCount(); i++) {
            if (adapter.getOriginal(i).getName().compareTo(name) == 0) {
                description = adapter.getOriginal(i).getFullText();
            }
        }
        Intent intent = new Intent(this, Db_Bestiary_Details.class);
        intent.putExtra("description", description);
        startActivity(intent);
    }
}
