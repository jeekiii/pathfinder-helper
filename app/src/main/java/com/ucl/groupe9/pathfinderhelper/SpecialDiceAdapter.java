package com.ucl.groupe9.pathfinderhelper;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SpecialDiceAdapter extends ArrayAdapter {

    private Context mContext;
    private ArrayList<Die> special_dice = new ArrayList<>();
    public int parentwidth=0;

    public SpecialDiceAdapter(Context context)
    {
        super(context,R.layout.content_multiple_dice);

        mContext = context;
        special_dice.add(new Die(4));
        special_dice.add(new Die(6));
        special_dice.add(new Die(8));
        special_dice.add(new Die(20));
        special_dice.add(new Die(100));

    }


    @Override
    public int getCount()
    {
        if (this.special_dice == null || this.special_dice.size() < 1) {
            return 0;
        } else {
            return this.special_dice.size();
        }
    }

    @Override
    public Die getItem(int position) {
        return special_dice.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View dieview;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView dievalue;
        ImageView dieBullet;

        if (convertView == null) {

            // get layout from mobile.xml
            dieview = inflater.inflate(R.layout.die_layout, parent,false);

            // if it's not recycled, initialize some attributes
            dievalue = (TextView) dieview.findViewById(R.id.die_value);

            // Set round_rect Shape color
            Drawable background = dieview.getBackground();
            if (background instanceof ShapeDrawable) {
                ((ShapeDrawable)background).getPaint().setColor(getItem(position).getColor());
            } else if (background instanceof GradientDrawable) {
                ((GradientDrawable)background).setColor(getItem(position).getColor());
            } else if (background instanceof ColorDrawable) {
                ((ColorDrawable)background).setColor(getItem(position).getColor());
            }

        } else {
            dieview = (View) convertView;
            dievalue = (TextView) dieview.findViewById(R.id.die_value);

        }


        ViewGroup.LayoutParams params = dieview.getLayoutParams();
        params.height = parentwidth/6;
        params.width = parentwidth/6;
        dieview.setLayoutParams(params);

        dieBullet = (ImageView) dieview.findViewById(R.id.dieBullets);

        if (getCount() > 1) {
            // Set the text
            Log.w("DEBUG", "size: " + special_dice.size() + " | " + getItem(position).getMaxValue());
            int val = getItem(position).getMaxValue();
            if (val == 6) {
                dieBullet.setVisibility(View.VISIBLE);
                dieBullet.setImageResource(R.drawable.nb6);
                dievalue.setText("");
            } else {
                dieBullet.setVisibility(View.INVISIBLE);
                dievalue.setText(Integer.toString(val));
            }
            // dieview.setBackgroundColor(getItem(position).getColor()); NO MORE NEEDED DUE TO background
        } else {
            dievalue.setText("?");
        }
        if(getItem(position).getMaxValue() == 100) {
            dievalue.setTextSize(dievalue.getTextSize()/2);
        }
        return dieview;
    }
}
