package com.ucl.groupe9.pathfinderhelper.static_databases;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ucl.groupe9.pathfinderhelper.R;
import com.ucl.groupe9.pathfinderhelper.model.Monster;

import java.util.ArrayList;
import java.util.Date;


public class Db_Bestiary_Adapter extends ArrayAdapter<Monster> implements Filterable {

    Context mContext;
    private ArrayList<Monster> originalData = null;
    private ArrayList<Monster>filteredData = null;
    private ItemFilter mFilter;

    public Db_Bestiary_Adapter(Context context, ArrayList<Monster> data) {
        super(context, R.layout.column_row_allmobs,data);
        this.originalData = new ArrayList<Monster>(data) ;
        this.filteredData = new ArrayList<Monster>(data) ;
        mFilter = new ItemFilter();
        mContext = context;
    }
    @Override
    public void clear() {
        this.filteredData = new ArrayList<Monster>(originalData);
    }

    public void add(Monster m) {filteredData.add(m);}

    public int getFullCount() {
        return originalData.size();
    }

    public Monster getOriginal(int position) {
        return originalData.get(position);
    }

    public int getCount() {
        return filteredData.size();
    }

    public Monster getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if(v == null)
        {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.column_row_allmobs,parent,false);
        }

        Monster m = filteredData.get(position);
        if (m != null) {

            TextView name = (TextView) v.findViewById(R.id.mobName);
            TextView source = (TextView) v.findViewById(R.id.mobSource);
            TextView cr = (TextView) v.findViewById(R.id.mobCR);

            name.setText(m.getName());
            source.setText(m.getSource());
            cr.setText(m.getCR());
        }

        return v;
    }

    @Override
    public Filter getFilter() {
        if( mFilter == null) {
            mFilter = new ItemFilter();
            return mFilter;
        } else {
            return mFilter;
        }
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            String prefix = constraint.toString().toLowerCase();

            if (prefix == null || prefix.length() == 0){
                ArrayList<Monster> list = new ArrayList<Monster>(originalData);
                results.values = list;
                results.count = list.size();
            } else {
                final ArrayList<Monster> list = new ArrayList<Monster>(originalData);
                final ArrayList<Monster> nlist = new ArrayList<>();
                int count = list.size();

                for (int i = 0; i<count; i++){
                    final Monster m = list.get(i);
                    final String value = m.getName().toLowerCase();

                    if(value.contains(prefix)){
                        nlist.add(m);
                    }
                    results.values = nlist;
                    results.count = nlist.size();
                }
            }

            Log.w("DEBUG","FOUND: results: "+results.count);

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Monster>) results.values;
            notifyDataSetChanged();
        }

    }
}
