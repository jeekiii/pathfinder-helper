package com.ucl.groupe9.pathfinderhelper;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 *
 */

public class DiceAdapter extends ArrayAdapter {

    Context mContext;

    private ArrayList<Die> dice_list = new ArrayList<>();

    private boolean rolled = false;

    private Randomiser random = null;


    public DiceAdapter(Context context,ArrayList<Integer> dice)
    {
        super(context,R.layout.die_layout,dice);
        this.mContext = context;
        if (dice != null && dice.size() > 0) {
            for (int i=0;i<dice.size();i++) {
                dice_list.add(new Die(dice.get(i).intValue()));
            }

        }
        random = new Randomiser();
    }

    public int dieValue(int position) {
        if (this.dice_list != null && this.dice_list.size() >0) {
            return this.dice_list.get(position).getValue();
        } else {
            return -1;
        }

    }

    public int dieMaxValue(int position) {
        if (this.dice_list != null && this.dice_list.size() >0) {
            return this.dice_list.get(position).getMaxValue();
        } else {
            return -1;
        }
    }

    @Override
    public int getCount()
    {
        if (this.dice_list == null || this.dice_list.size() < 1) {
            return 0;
        } else return this.dice_list.size();
    }

    @Override
    public Die getItem(int position) {
        return this.dice_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View dieview;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView dievalue;
        ImageView dieBullet;
        if (convertView == null) {

            dieview = inflater.inflate(R.layout.die_layout,parent,false);

        } else {
            dieview = (View) convertView;
        }
        dievalue = (TextView) dieview.findViewById(R.id.die_value);
        dieBullet = (ImageView) dieview.findViewById(R.id.dieBullets);
        dievalue.setText("?");

        // Set round_rect Shape color
        Drawable background = dieview.getBackground();
        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable)background).getPaint().setColor(getItem(position).getColor());
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable)background).setColor(getItem(position).getColor());
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable)background).setColor(getItem(position).getColor());
        }


        // Set appropriate die value
        if (getCount() < 1) {
            Log.w("DEBUG","error dice_list is null");
        } else {
            if (rolled && dieValue(position) > 0) {
                dievalue.setText(Integer.toString(dieValue(position)));
                if(dieMaxValue(position) == 6) {
                    switch (dieValue(position)) {
                        case 1: dieBullet.setImageResource(R.drawable.nb1);
                            break;
                        case 2: dieBullet.setImageResource(R.drawable.nb2);
                            break;
                        case 3: dieBullet.setImageResource(R.drawable.nb3);
                            break;
                        case 4: dieBullet.setImageResource(R.drawable.nb4);
                            break;
                        case 5: dieBullet.setImageResource(R.drawable.nb5);
                            break;
                        case 6: dieBullet.setImageResource(R.drawable.nb6);
                            break;
                        default: dieBullet.setImageResource(R.drawable.nb4);
                            break;
                    }
                    dievalue.setText("");
                }
            } else {
                dieBullet.setVisibility(View.VISIBLE);
            }
        }

        if(dieValue(position) == 100) {
            dievalue.setTextSize(dievalue.getTextSize()/2);
        }

        if (dieMaxValue(position) == 6) {
            if (!rolled) {
                dieBullet.setVisibility(View.INVISIBLE);
                dievalue.setText("?");
            }else {
                dieBullet.setVisibility(View.VISIBLE);
            }
        } else {
            dieBullet.setVisibility(View.INVISIBLE);
        }
        return dieview;
    }

    /*
        Remove the die at position position in the dice_list list
     */
    public void remove(int position) {
        dice_list.remove(position);
        this.notifyDataSetChanged();
        Log.w("DEBUG", "Dice on position " + position + " removed");
    }

    /*
        Add a die with maximum value max_value in the dice_list list
     */
    public void add(int max_value) {
        this.dice_list.add(new Die(max_value));
        this.notifyDataSetChanged();
        Log.w("DEBUG", "Dice ("+max_value+") added");
        return;
    }

    /*
        Remove all the dice in the dice_list list
     */
    public void clear() {
        this.rolled = false;
        this.dice_list = new ArrayList<Die>();
        this.notifyDataSetChanged();
        Log.w("DEBUG", "All dice cleared");
        return;
    }

    /*
        Reset the dice in the dice_list list
     */
    public void reset() {
        for (int i=0;i<getCount();i++) {
            dice_list.get(i).setValue(0);
        }
        this.rolled = false;
        this.notifyDataSetChanged();
        Log.w("DEBUG", "reset dice");
        return;
    }

    /*
        Get a random value of the dice and update the display
     */
    public void rollDice() {
        if (this.dice_list == null || this.dice_list.size() == 0) {
            Log.w("DEBUG","Dice not rolled because no dice");
            return;
        }else {
            random.get_multiple_die_value(getCount(),this.dice_list);
            Log.w("DEBUG", "Dice rolled");
            this.notifyDataSetChanged();
        }
        this.rolled = true;
        return;
    }
}
