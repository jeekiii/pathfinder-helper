package com.ucl.groupe9.pathfinderhelper.static_databases;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.ucl.groupe9.pathfinderhelper.R;


public class Db_Feats_Details extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db_feats_details);
        Intent intent = getIntent();
        String details = intent.getStringExtra("description");
        if(details == "")
            details = "no description";
        WebView tv = (WebView) findViewById(R.id.featDetails);
        tv.loadData(details, "text/html", "UTF-8");
    }


}
