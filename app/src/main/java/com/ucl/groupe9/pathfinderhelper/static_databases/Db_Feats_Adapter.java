package com.ucl.groupe9.pathfinderhelper.static_databases;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ucl.groupe9.pathfinderhelper.R;
import com.ucl.groupe9.pathfinderhelper.model.Feat;

import java.util.ArrayList;


public class Db_Feats_Adapter extends ArrayAdapter<Feat> implements Filterable {

    Context mContext;
    private ArrayList<Feat> originalData = null;
    private ArrayList<Feat>filteredData = null;
    private ItemFilter mFilter;

    public Db_Feats_Adapter(Context context, ArrayList<Feat> data) {
        super(context, R.layout.column_row_allfeats,data);
        this.originalData = new ArrayList<Feat>(data) ;
        this.filteredData = new ArrayList<Feat>(data) ;
        mFilter = new ItemFilter();
        mContext = context;
    }
    @Override
    public void clear() {
        this.filteredData = new ArrayList<Feat>(originalData);
    }

    public void add(Feat m) {filteredData.add(m);}

    public int getFullCount() {
        return originalData.size();
    }

    public Feat getOriginal(int position) {
        return originalData.get(position);
    }

    public int getCount() {
        return filteredData.size();
    }

    public Feat getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if(v == null)
        {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.column_row_allfeats,parent,false);
        }

        Feat m = filteredData.get(position);
        if (m != null) {

            TextView name = (TextView) v.findViewById(R.id.featName);
            TextView type = (TextView) v.findViewById(R.id.featType);
            //TextView prerequisite_feats = (TextView) v.findViewById(R.id.featPrerequisiteFeats);

            name.setText(m.getName());
            type.setText(m.gettype());
            //prerequisite_feats.setText(m.getprequisite_feats());
        }

        return v;
    }

    @Override
    public Filter getFilter() {
        if( mFilter == null) {
            mFilter = new ItemFilter();
            return mFilter;
        } else {
            return mFilter;
        }
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            String prefix = constraint.toString().toLowerCase();

            if (prefix == null || prefix.length() == 0){
                ArrayList<Feat> list = new ArrayList<Feat>(originalData);
                results.values = list;
                results.count = list.size();
            } else {
                final ArrayList<Feat> list = new ArrayList<Feat>(originalData);
                final ArrayList<Feat> nlist = new ArrayList<>();
                int count = list.size();

                for (int i = 0; i<count; i++){
                    final Feat m = list.get(i);
                    final String value = m.getName().toLowerCase();

                    if(value.contains(prefix)){
                        nlist.add(m);
                    }
                    results.values = nlist;
                    results.count = nlist.size();
                }
            }

            Log.w("DEBUG","FOUND: results: "+results.count);

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Feat>) results.values;
            notifyDataSetChanged();
        }

    }
}
