package com.ucl.groupe9.pathfinderhelper.static_databases;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ucl.groupe9.pathfinderhelper.R;
import com.ucl.groupe9.pathfinderhelper.model.MagicItem;

import java.util.ArrayList;


public class Db_MagicItems_Adapter extends ArrayAdapter<MagicItem> implements Filterable {

    Context mContext;
    private ArrayList<MagicItem> originalData = null;
    private ArrayList<MagicItem>filteredData = null;
    private ItemFilter mFilter;

    public Db_MagicItems_Adapter(Context context, ArrayList<MagicItem> data) {
        super(context, R.layout.column_row_allmagicitems,data);
        this.originalData = new ArrayList<MagicItem>(data) ;
        this.filteredData = new ArrayList<MagicItem>(data) ;
        mFilter = new ItemFilter();
        mContext = context;
    }
    @Override
    public void clear() {
        this.filteredData = new ArrayList<MagicItem>(originalData);
    }

    public void add(MagicItem m) {filteredData.add(m);}

    public int getFullCount() {
        return originalData.size();
    }

    public MagicItem getOriginal(int position) {
        return originalData.get(position);
    }

    public int getCount() {
        return filteredData.size();
    }

    public MagicItem getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if(v == null)
        {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.column_row_allmagicitems,parent,false);
        }

        MagicItem m = filteredData.get(position);
        if (m != null) {

            TextView Name = (TextView) v.findViewById(R.id.Name);
            TextView Aura = (TextView) v.findViewById(R.id.Aura);
            //TextView Price = (TextView) v.findViewById(R.id.Price);

            Name.setText(m.getName());
            Aura.setText(m.getAura());
            //Price.setText(m.getPrice());
        }

        return v;
    }

    @Override
    public Filter getFilter() {
        if( mFilter == null) {
            mFilter = new ItemFilter();
            return mFilter;
        } else {
            return mFilter;
        }
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            String prefix = constraint.toString().toLowerCase();

            if (prefix == null || prefix.length() == 0){
                ArrayList<MagicItem> list = new ArrayList<MagicItem>(originalData);
                results.values = list;
                results.count = list.size();
            } else {
                final ArrayList<MagicItem> list = new ArrayList<MagicItem>(originalData);
                final ArrayList<MagicItem> nlist = new ArrayList<>();
                int count = list.size();

                for (int i = 0; i<count; i++){
                    final MagicItem m = list.get(i);
                    final String value = m.getName().toLowerCase();

                    if(value.contains(prefix)){
                        nlist.add(m);
                    }
                    results.values = nlist;
                    results.count = nlist.size();
                }
            }

            Log.w("DEBUG","FOUND: results: "+results.count);

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<MagicItem>) results.values;
            notifyDataSetChanged();
        }

    }
}
