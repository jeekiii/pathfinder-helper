package com.ucl.groupe9.pathfinderhelper;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class AbilityScores extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


    private List<HashMap<String, String>> fillMaps;
    private Integer maxId;
    private SimpleAdapter adapter;
    private TextView textTotal;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ability_scores);

        maxId = Integer.valueOf(0);
        spinner = (Spinner) findViewById(R.id.points_spinner);
        spinner.setOnItemSelectedListener(this);
        final ListView list = (ListView) findViewById(R.id.abilityList);
        textTotal = (TextView) findViewById(R.id.textTotal);

        //to store all the data.
        String[] from = new String[] {"ability", "total", "base", "racial", "id"};
        int[] to = new int[] { R.id.textAbility, R.id.textScore, R.id.numberPickBase, R.id.numberPickRacialModifier, R.id.textIdAbility};
        fillMaps = new ArrayList<>();
        // initialize the data

        String[] names = new String[] {"Str", "Dex", "Cons", "Int", "Wis", "Cha"};
        for (String name : names) {
            HashMap<String, String> map = new HashMap<>();

            map.put("ability", name);
            map.put("total", "10");
            map.put("base", "10");
            map.put("racial", "0");
            map.put("id", maxId.toString());
            fillMaps.add(map);
            maxId++;
        }
        adapter = new SimpleAdapter(this, fillMaps, R.layout.column_row_ability_scores, from, to);
        list.setAdapter(adapter);
        setTotal();
        
    }

    public void setTotal()
    {
        int total = 0;
        String text = spinner.getSelectedItem().toString();
        if(text.compareTo("low fantasy (10)")==0){total+=10;}
        else if(text.compareTo("standard fantasy (15)")==0){total+=15;}
        else if(text.compareTo("high fantasy (20)")==0){total+=20;}
        else if(text.compareTo("epic fantasy (25)")==0){total+=25;}
        int score;
        int i;
        for(i = 0; i < fillMaps.size(); i++)
        {
            HashMap<String, String> map = fillMaps.get(i);
            try
            {
                score = Integer.parseInt(map.get("base"));
            }
            catch(NumberFormatException e)
            {
                System.out.println(map.get("base")+ "whatafak");
                score = 10;
            }
            switch(score)
            {
                case 7:
                    total += 4;
                    break;
                case 8:
                    total +=2;
                    break;
                case 9:
                    total +=1;
                    break;
                case 10:
                    total +=0;
                    break;
                case 11:
                    total -=1;
                    break;
                case 12:
                    total -=2;
                    break;
                case 13:
                    total -=3;
                    break;
                case 14:
                    total -=5;
                    break;
                case 15:
                    total -=7;
                    break;
                case 16:
                    total -=10;
                    break;
                case 17:
                    total -=13;
                    break;
                case 18:
                    total -=17;
                break;
            }

        }
        textTotal.setText("Points restants: " + ((Integer) total).toString());
        Button ok = (Button) findViewById(R.id.buttonOK);
        if(total==0) {
            ok.setClickable(true);
        }
        else {
            ok.setClickable(false);
        }
    }
    public void abilityButtonClick(View v)
    {
        LinearLayout ParentRow = (LinearLayout)v.getParent();
        int id = Integer.parseInt(((TextView) ParentRow.getChildAt(4)).getText().toString());
        changeButtonClick(true, v, id);
    }
    public void racialButtonClick(View v)
    {
        LinearLayout ParentRow = (LinearLayout)v.getParent();
        int id = Integer.parseInt(((TextView) ParentRow.getChildAt(4)).getText().toString());
        changeButtonClick(false, v, id);
    }
    public void changeButtonClick(final boolean isBase, View v, final int id)
    {

        //int value;
        final Dialog d = new Dialog(AbilityScores.this);
        d.setTitle("NumberPicker");
        d.setContentView(R.layout.number_dialog);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        HashMap<String, String> map = fillMaps.get(id);

        if(isBase) {
            np.setMaxValue(18); // max value 100
            np.setMinValue(7);   // min value 0
            np.setValue(Integer.parseInt(map.get("base")));
        }
        else{
            //warning: complete bullshit to overwrite shitty useless restrictions of android below
            //for real, what the fuck. They accept string but not negative integers?
            np.setMinValue(0);
            np.setMaxValue(8);
            np.setValue(Integer.parseInt(map.get("racial"))+4);
            np.setFormatter(new NumberPicker.Formatter() {
                @Override
                public String format(int index) {
                    return Integer.toString(index - 4);
                }


            });
            try//are you fucking shitting me? More bullshit to make that idiocy work? This is probably amongst the most disgusting hacks I've done... And I once coded a >1000 lines of code function in 5th grade.
            {
                Method method = np.getClass().getDeclaredMethod("changeValueByOne", boolean.class);
                method.setAccessible(true);
                method.invoke(np, true);
            }
            catch(Exception e) {
                e.printStackTrace(); //a lot more general and shorter for the same behaviour
            }
            /*catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            catch (NoSuchMethodException e) {//wut? Isn't it a bit fucked when it compiles and gives this exception? Funnily enough this is the only one NOT yellow in this software.
                e.printStackTrace();
            }*/

        }
        np.setWrapSelectorWheel(false);
        Button b1 = (Button) d.findViewById(R.id.button1);
        Button b2 = (Button) d.findViewById(R.id.button2);
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                HashMap<String, String> map = fillMaps.get(id);
                if(isBase) {
                    map.put("base", ((Integer) np.getValue()).toString());
                }
                else
                {
                    map.put("racial", ((Integer) (np.getValue() - 4)).toString());
                }
                    map.put("total", ((Integer) (Integer.parseInt(map.get("base")) + Integer.parseInt(map.get("racial")))).toString());//simply an addition of the two
                fillMaps.set(id, map);
                adapter.notifyDataSetChanged();
                setTotal();
                d.dismiss();

            }

        });


        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });
        d.show();
    }
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        setTotal();
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    public void onClickOK(View v) {
        //Spinner s = (Spinner) findViewById(R.id.points_spinner);
        ListView listView = (ListView) findViewById(R.id.abilityList);
        //s.setClickable(false);
        //listView.setClickable(false);
        Intent intent = new Intent(this, CharacterSheet.class);
        for(int count = 0; count < listView.getCount(); count++) {
            LinearLayout ly = (LinearLayout) listView.getChildAt(count);
            TextView tv = (TextView) ly.getChildAt(0);
            TextView et = (TextView) ly.getChildAt(1);
            intent.putExtra(tv.getText().toString(), Integer.parseInt(et.getText().toString()));
        }
        startActivity(intent);
    }

}
