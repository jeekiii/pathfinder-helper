package com.ucl.groupe9.pathfinderhelper.model;

import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Arrays;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ucl.groupe9.pathfinderhelper.puppeteer.MobSQLiteHelper;

/**
 * Created by fair on 3/9/16.
 */
public class Monster {

    /*
     *
     * /!\ Important note for those reading this source code :
     * "Mob" is a common diminutive for the word "Monster" in video games.
     * We thus interchange both words a lot in this code, as they are synonyms.
     *
     */



    /*
    Some useful column names in the database
     */
    public static final String DB_TABLE_BESTIARY = "BESTIARY";
    public static final String DB_COL_Name = "Name";
    public static final String DB_COL_CR = "CR";
    public static final String DB_COL_XP = "XP";
    public static final String DB_COL_Race = "Race";
    public static final String DB_COL_id = "id";
    public static final String DB_COL_FullText = "FullText";

    public static final String DB_COL_Source = "Source";
    /*
    Just to be safe, we use the notation tableName.columnName
     */
    public static final String DB_COL_BESTIARY_ID = DB_TABLE_BESTIARY + "." + DB_COL_id;
    /*
    Name of the column we order our elements by
     */
    public static String order_by = DB_COL_Name;
    /*
    Sorting order : ASC for ascending, DESC for descending
     */
    public static String order = "ASC";

    /* Name and other useful stuff */
    private String Name;
    private String CR;
    private String XP;
    private String Race;
    private String Class;
    private String MonsterSource;
    private String Alignment;
    private String Size;
    private String Type;
    private String SubType;
    private String Init;
    private String Senses;
    private String Aura;
    private String AC;
    private String AC_Mods;
    private String HP;
    private String HD;
    private String HP_Mods;
    private String Saves;
    private String Fort;
    private String Ref;
    private String Will;
    private String Save_Mods;
    private String DefensiveAbilities;
    private String DR;
    private String Immune;
    private String Resist;
    private String SR;
    private String Weaknesses;
    private String Speed;
    private String Speed_Mod;
    private String Melee;
    private String Ranged;
    private String Space;
    private String Reach;
    private String SpecialAttacks;
    private String SpellLikeAbilities;
    private String SpellsKnown;
    private String SpellsPrepared;
    private String SpellDomains;
    private String AbilityScores;
    private String AbilityScore_Mods;
    private String BaseAtk;
    private String CMB;
    private String CMD;
    private String Feats;
    private String Skills;
    private String RacialMods;
    private String Languages;
    private String SQ;
    private String Environment;
    private String Organization;
    private String Treasure;
    private String Description_Visual;
    private String Group;
    private String Source;
    private String IsTemplate;
    private String SpecialAbilities;
    private String Description;
    private String FullText;
    private String Gender;
    private String Bloodline;
    private String ProhibitedSchools;
    private String BeforeCombat;
    private String DuringCombat;
    private String Morale;
    private String Gear;
    private String OtherGear;
    private String Vulnerability;
    private String Note;
    private String CharacterFlag;
    private String CompanionFlag;
    private String Fly;
    private String Climb;
    private String Burrow;
    private String Swim;
    private String Land;
    private String TemplatesApplied;
    private String OffenseNote;
    private String BaseStatistics;
    private String ExtractsPrepared;
    private String AgeCategory;;
    private String Mystery;
    private String ClassArchetypes;
    private String Patron;
    private String CompanionFamiliarLink;
    private String FocusedSchool;
    private String Traits;
    private String AlternateNameForm;
    private String LinkText;
    private final int id;  /* PRIMARY KEY */ /* PRIMARY KEY */ /* PRIMARY KEY */ /* PRIMARY KEY */ /* PRIMARY KEY */ /* PRIMARY KEY */ /* PRIMARY KEY */
    private String UniqueMonster;
    private String ThassilonianSpecialization;
    private String Variant;
    private String MR;
    private String Mythic;
    private String MT;
    private String Inquisition;
    private String Spirit;
    private String PsychicMagic;
    private String KineticistWildTalents;
    private String Implements;
    private String PsychicDiscipline;

    public String getName() {
        return Name;
    }

    public String getCR() {
        return CR;
    }

    public String getXP() {
        return XP;
    }

    public String getRace() {
        return Race;
    }

    public String getMobClass() { //GetClass already exists!
        return Class;
    }

    public String getMonsterSource() {
        return MonsterSource;
    }

    public String getAlignment() {
        return Alignment;
    }

    public String getSize() {
        return Size;
    }

    public String getType() {
        return Type;
    }

    public String getSubType() {
        return SubType;
    }

    public String getInit() {
        return Init;
    }

    public String getSenses() {
        return Senses;
    }

    public String getAura() {
        return Aura;
    }

    public String getAC() {
        return AC;
    }

    public String getAC_Mods() {
        return AC_Mods;
    }

    public String getHP() {
        return HP;
    }

    public String getHD() {
        return HD;
    }

    public String getHP_Mods() {
        return HP_Mods;
    }

    public String getSaves() {
        return Saves;
    }

    public String getFort() {
        return Fort;
    }

    public String getRef() {
        return Ref;
    }

    public String getWill() {
        return Will;
    }

    public String getSave_Mods() {
        return Save_Mods;
    }

    public String getDefensiveAbilities() {
        return DefensiveAbilities;
    }

    public String getDR() {
        return DR;
    }

    public String getImmune() {
        return Immune;
    }

    public String getResist() {
        return Resist;
    }

    public String getSR() {
        return SR;
    }

    public String getWeaknesses() {
        return Weaknesses;
    }

    public String getSpeed() {
        return Speed;
    }

    public String getSpeed_Mod() {
        return Speed_Mod;
    }

    public String getMelee() {
        return Melee;
    }

    public String getRanged() {
        return Ranged;
    }

    public String getSpace() {
        return Space;
    }

    public String getReach() {
        return Reach;
    }

    public String getSpecialAttacks() {
        return SpecialAttacks;
    }

    public String getSpellLikeAbilities() {
        return SpellLikeAbilities;
    }

    public String getSpellsKnown() {
        return SpellsKnown;
    }

    public String getSpellsPrepared() {
        return SpellsPrepared;
    }

    public String getSpellDomains() {
        return SpellDomains;
    }

    public String getAbilityScores() {
        return AbilityScores;
    }

    public String getAbilityScore_Mods() {
        return AbilityScore_Mods;
    }

    public String getBaseAtk() {
        return BaseAtk;
    }

    public String getCMB() {
        return CMB;
    }

    public String getCMD() {
        return CMD;
    }

    public String getFeats() {
        return Feats;
    }

    public String getSkills() {
        return Skills;
    }

    public String getRacialMods() {
        return RacialMods;
    }

    public String getLanguages() {
        return Languages;
    }

    public String getSQ() {
        return SQ;
    }

    public String getEnvironment() {
        return Environment;
    }

    public String getOrganization() {
        return Organization;
    }

    public String getTreasure() {
        return Treasure;
    }

    public String getDescription_Visual() {
        return Description_Visual;
    }

    public String getGroup() {
        return Group;
    }

    public String getSource() {
        return Source;
    }

    public String getIsTemplate() {
        return IsTemplate;
    }

    public String getSpecialAbilities() {
        return SpecialAbilities;
    }

    public String getDescription() {return Description;}

    public String getFullText() {
        return FullText;
    }

    public String getGender() {
        return Gender;
    }

    public String getBloodline() {
        return Bloodline;
    }

    public String getProhibitedSchools() {
        return ProhibitedSchools;
    }

    public String getBeforeCombat() {
        return BeforeCombat;
    }

    public String getDuringCombat() {
        return DuringCombat;
    }

    public String getMorale() {
        return Morale;
    }

    public String getGear() {
        return Gear;
    }

    public String getOtherGear() {
        return OtherGear;
    }

    public String getVulnerability() {
        return Vulnerability;
    }

    public String getNote() {
        return Note;
    }

    public String getCharacterFlag() {
        return CharacterFlag;
    }

    public String getCompanionFlag() {
        return CompanionFlag;
    }

    public String getFly() {
        return Fly;
    }

    public String getClimb() {
        return Climb;
    }

    public String getBurrow() {
        return Burrow;
    }

    public String getSwim() {
        return Swim;
    }

    public String getLand() {
        return Land;
    }

    public String getTemplatesApplied() {
        return TemplatesApplied;
    }

    public String getOffenseNote() {
        return OffenseNote;
    }

    public String getBaseStatistics() {
        return BaseStatistics;
    }

    public String getExtractsPrepared() {
        return ExtractsPrepared;
    }

    public String getAgeCategory() {
        return AgeCategory;
    }

    public String getMystery() {
        return Mystery;
    }

    public String getClassArchetypes() {
        return ClassArchetypes;
    }

    public String getPatron() {
        return Patron;
    }

    public String getCompanionFamiliarLink() {
        return CompanionFamiliarLink;
    }

    public String getFocusedSchool() {
        return FocusedSchool;
    }

    public String getTraits() {
        return Traits;
    }

    public String getAlternateNameForm() {
        return AlternateNameForm;
    }

    public String getLinkText() {
        return LinkText;
    }

    public int getId() {
        return id;
    }

    public String getUniqueMonster() {
        return UniqueMonster;
    }

    public String getThassilonianSpecialization() {
        return ThassilonianSpecialization;
    }

    public String getVariant() {
        return Variant;
    }

    public String getMR() {
        return MR;
    }

    public String getMythic() {
        return Mythic;
    }

    public String getMT() {
        return MT;
    }

    public String getInquisition() {
        return Inquisition;
    }

    public String getSpirit() {
        return Spirit;
    }

    public String getPsychicMagic() {
        return PsychicMagic;
    }

    public String getKineticistWildTalents() {
        return KineticistWildTalents;
    }

    public String getImplements() {
        return Implements;
    }

    public String getPsychicDiscipline() {
        return PsychicDiscipline;
    }


    /**
     * Constructor. Puts an element from our database into our SparseArray or HashMap..
     */
    private Monster(int mobId) {

        // On enregistre l'id dans la variable d'instance.
        this.id = mobId;

        // On enregistre l'instance de l'élément de collection courant dans la hashMap.
        Monster.mobSparseArray.put(mobId, this);

        // On charge les données depuis la base de données.
        loadData();
    }


    /**
     * (Re)charge les informations depuis la base de données.
     *
     * @pre L'id de l'élément est indiqué dans this.id et l'élément existe dans la base de données.
     * @post Les informations de l'élément sont chargées dans les variables d'instance de la
     * classe.
     */
    private void loadData() {
        // Récupération de la base de données en mode "lecture".

        SQLiteDatabase db = MobSQLiteHelper.get().getReadableDatabase();
        // Colonnes pour lesquelles il nous faut les données.
        String[] columns = new String[]{DB_COL_Name, DB_COL_CR, DB_COL_XP, DB_COL_Race, DB_COL_Source, DB_COL_FullText};

        // Critères de sélection de la ligne :
        String selection = DB_COL_id + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(id)};

        // Requête SELECT à la base de données.
        /*
        System.out.println("CheckpointLoadData5");
        System.out.println("Table: " + DB_TABLE_BESTIARY);
        System.out.println("Columns: " + Arrays.toString(columns));
        System.out.println("Selection: " + selection);
        System.out.println("SelectionArgs: " + Arrays.toString(selectionArgs));
        */
        Cursor c = db.query(DB_TABLE_BESTIARY, columns, selection, selectionArgs, null, null, null);

        // Placement du curseur sur le  premier résultat (ici le seul puisque l'objet est unique).
        c.moveToFirst();


        if(c.getCount() > 0)
        {
            // Copie des données de la ligne vers les variables d'instance de l'objet courant.
            this.Name = c.getString(0);
            this.CR = c.getString(1);
            this.XP = c.getString(2);
            this.Race = c.getString(3);
            this.Source = c.getString(4);
            this.FullText = c.getString(5);
        }


        // Fermeture du curseur.
        c.close();
        db.close();
    }


    /******************************************************************************
     * STATIC part of class
     ******************************************************************************/
    /**
     * Contains the already existing instances of the objects to avoid creating two
     * instances of the same object.
     */
    private static final SparseArray<Monster> mobSparseArray = new SparseArray<Monster>();

    /*
     * Creates a new element (Monster) in the database
     *
     * @param param         Way too many (102)
     *
     * @return True in case of success, false in case of failure.
     * @post Registers a new element in the database
     */
    public static boolean create(String param) {
        /* May be implemented later, may just be deleted. Would require 102 parameters. */
        return false;
    }

    /**
     * Deletes an element in the database
     *
     * @param Id        Id of the monster to be erased
     *
     * @return True in case of success, false in case of failure
     * @post Deletes element from database
     */
    public static boolean delete(String Id) {
        return false;
    }

    /**
     * Inverse l'ordre de tri actuel.
     *
     * @pre La valeur de Song.order est soit ASC soit DESC.
     * @post La valeur de Song.order a été modifiée et est soit ASC soit DESC.
     */
    public static void reverseOrder() {
        if (Monster.order.equals("ASC")) {
            Monster.order = "DESC";
        } else {
            Monster.order = "ASC";
        }
    }

    /**
     * Provides the list of all in the elements in the current user collection containing
     * searchQuery in its name.
     *
     * @param searchQuery Search query.
     *
     * @return List of elements of collection corresponding to the search query.
     */
    public static ArrayList<Monster> searchMobs(String searchQuery) {
        // Selection criteria (WHERE) : have a name corresponding to the search query
        String selection = DB_COL_Name + " LIKE ?";
        String[] selectionArgs = new String[]{"%" + searchQuery + "%"};

        // Selection criteria passed onto the submethod retrieving the elements.
        return getMobs(selection, selectionArgs);
    }

    /*
     * Provides a list of all available monsters
     *
     * @return List of elements
     */

    public static ArrayList<Monster> getMobs() {
        // The arguments passed to the following function are the selection criteria for the recuperation method.
        return getMobs(null, null);
    }

    /**
     * Provides an instance of an element (Monster) in the database. If the element doesn't already
     * have an instance, a new instance is created.
     *
     * @param ciId Id of the monster
     *
     * @return An instance of the monster
     * @pre The element corresponding to the Id must exist in the database
     */
    public static Monster get(int ciId) { //ciId for Collection Item ID
        if(mobSparseArray==null){ return null;}
        else{
        Monster ci = Monster.mobSparseArray.get(ciId);
        if (ci != null) {
            return ci;
        }
        return new Monster(ciId);}

    }

    /**
     * Provides the list of all objects corresponding to the search criteria.
     *
     * This method is a submethod of getMobs and searchMobs.
     *
     * @param selection     A filter declaring which elements to return, using the format of the
     *                      SQL WHERE clause (excluding the WHERE itself). Using null will return
     *                      all the elements.
     * @param selectionArgs You can include ? in selection, which will be replaced by the values
     *                      in selectionArgs, in their order of apparition in selection.
     *                      These values will be linked as chains.
     *
     * @return List of objects. The list can be empty if no object corresponds.
     */
    public static ArrayList<Monster> getMobs(String selection, String[] selectionArgs) {
        // Initialisation of the song list.
        ArrayList<Monster> mobs = new ArrayList<Monster>();

        // Retrieval of the SQLiteHelper to get the database.
        SQLiteDatabase db = MobSQLiteHelper.get().getReadableDatabase();

        // Columns to be retrieved. Here we only use the id of the element, the rest will be
        // retrieved through loadData() when an instance of the element is created. (this is a development choice)
        String[] columns = new String[]{DB_COL_BESTIARY_ID};


        //System.out.println("IN GETMOBS : COLUMNS[0] : " + columns[0]);

        // SELECT request to the database
        Cursor c = db.query(DB_TABLE_BESTIARY, columns, selection, selectionArgs, null, null, Monster.order_by + " " + Monster.order);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            // ID of the element
            int curID = c.getInt(0); //91 is the column where the IDs are.
            // The instance of the element of collection is retrieved with the get(curID) method
            // (If the instance doesn't exist yet, it is created through the get method)
            //System.out.println("ID IN GETMOBS WITH GETINT(0) : " + curID);
            Monster mob = Monster.get(curID);

            // Addition of the element of collection to the list
            mobs.add(mob);

            c.moveToNext();
        }

        // Closing of the cursor and of the database
        c.close();
        db.close();

        return mobs;
    }

}
