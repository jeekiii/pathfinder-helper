package com.ucl.groupe9.pathfinderhelper;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.app.ListActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static android.app.PendingIntent.getActivity;
import static com.ucl.groupe9.pathfinderhelper.R.layout.column_row_initiative_roller;
import static com.ucl.groupe9.pathfinderhelper.R.layout.content_initiative_roller;

public class InitiativeRoller extends AppCompatActivity {

    private Integer initModifier; // the modifier we add to the total, it's user-generated
    private int initTotal; // the total, it's random 1-20 + modifier
    private EditText editInitModifierField; // the field to edit the initModifier
    private TextView textInitTotalField; // the text field witht the total
    private Random rng;

    private Toolbar toolbar;
    private SimpleAdapter adapter;
    private Integer maxId;
    private List<HashMap<String, String>> fillMaps;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        rng = new Random();
        initModifier = 0;
        initTotal = 0;
        maxId = 0;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initiative_roller);

        //only thing about the toolbar, you don't have to do anything else
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Button rollButton = (Button) findViewById(R.id.button);// the button to roll
        Button addButton = (Button) findViewById(R.id.buttonAdd);

        //the list
        final ListView list = (ListView) findViewById(R.id.list);

        //to store all the data.
        String[] from = new String[] {"name", "base", "total", "id"};
        int[] to = new int[] { R.id.editName, R.id.editModifier, R.id.textTotal, R.id.textId};
        fillMaps = new ArrayList<HashMap<String, String>>();
        // initialize the data
        for(int i = 0; i <1; i++){
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("name", "");
            map.put("base", "");
            map.put("total", "0");
            map.put("id", maxId.toString());


            fillMaps.add(map);

            maxId++;
        }

        //if you remove this, the whole list stops showing, so don't? I guess? first lines fills the list, second shows it,
        adapter = new SimpleAdapter(this, fillMaps, R.layout.column_row_initiative_roller, from, to);
        list.setAdapter(adapter);


        //would be clearer if I could just move this elsewhere --" I've no idea how this shit works.
        rollButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                fillMaps.clear();
                System.out.println(list.getAdapter().getCount() + "count");
                String value = "";
                ArrayList<String> valueList = new ArrayList<String>();
                for (int i = 0; i < list.getChildCount(); i++) {
                    LinearLayout layout = (LinearLayout) list.getChildAt(i);

                    String text = ((EditText) layout.getChildAt(1)).getText().toString();
                    try{
                        initModifier = Integer.parseInt(text);
                    }
                    catch (NumberFormatException e)
                    {
                        if(text.equals(""))
                        {
                            initModifier = 0;
                        }
                        else
                        {
                            //this means he somehow wrote weird shit there Oo?
                            initModifier = 0;
                        }
                    }
                    Integer total = initModifier + rng.nextInt(20)+1;
                    ((TextView) layout.getChildAt(2)).setText(total.toString());

                    //this is to fill the items map with updated values.
                    Integer id;
                    try {
                        id = Integer.parseInt(((TextView) layout.getChildAt(3)).getText().toString());//maybe put this in a try/catch
                    }
                    catch (NumberFormatException e)
                    {
                        System.out.println("idtext: " + ((TextView) layout.getChildAt(3)).getText());
                        id = 0;
                    }
                    String name = ((TextView)layout.getChildAt(0)).getText().toString();
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("name", name);
                    map.put("base", initModifier.toString());
                    map.put("total", total.toString());
                    map.put("id", id.toString());
                    fillMaps.add(map);

                }
                //now reordering the list
                for(int i = 0; i < fillMaps.size(); i++)
                {
                    int currentMaxId = 0;
                    int currentMaxTotal = 0;
                    int tempTotal = 0;
                    HashMap<String, String> tempMap;
                    HashMap<String, String> currentMap;
                    tempMap = fillMaps.get(i);
                    currentMaxId = i;
                    currentMaxTotal = 0;
                    for(int j = i; j < fillMaps.size(); j++)
                    {
                        tempMap = fillMaps.get(j);

                        try
                        {
                           tempTotal = Integer.parseInt(tempMap.get("total"));
                        }
                        catch(NumberFormatException e)
                        {
                            tempTotal = 0;
                        }
                        if(tempTotal > currentMaxTotal)
                        {
                            currentMaxTotal = tempTotal;
                            currentMaxId = j;
                        }
                    }
                    tempMap = fillMaps.get(currentMaxId);
                    fillMaps.set(currentMaxId, fillMaps.get(i));
                    fillMaps.set(i, tempMap);
                }
                adapter.notifyDataSetChanged();

            }

        });
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {



                fillMaps.clear();
                HashMap<String, String> map;
                for(int i = 0; i < list.getChildCount(); i++)
                {
                    LinearLayout layout = (LinearLayout) list.getChildAt(i);

                    map = new HashMap<String, String>();
                    String base = ((EditText) layout.getChildAt(1)).getText().toString();//modifier
                    String total = ((TextView)layout.getChildAt(2)).getText().toString();//total
                    String id =  ((TextView) layout.getChildAt(3)).getText().toString();//id
                    String name = ((TextView)layout.getChildAt(0)).getText().toString();
                    map.put("name", name);
                    map.put("base", base);
                    map.put("total", total);
                    map.put("id", id);
                    fillMaps.add(map);
                }
                map = new HashMap<String, String>();
                map.put("name", "");
                map.put("base", "");
                map.put("total", "0");
                map.put("id", maxId.toString());

                maxId++;
                fillMaps.add(map);
                adapter.notifyDataSetChanged();
            }
        });


    }
    public void deleteButtonClick(View v)
    {


        HashMap<String, String> temp;

        //get the row the clicked button is in
        LinearLayout ParentRow = (LinearLayout)v.getParent();
        ListView parentList = (ListView)ParentRow.getParent();
        fillMaps.clear();
        HashMap<String, String> map;
        for(int i = 0; i < parentList.getChildCount(); i++)//There must be a more efficient way to do this. This makes sure we don't lose user data that hasn't been saved in fillMaps
        {
            LinearLayout layout = (LinearLayout) parentList.getChildAt(i);

            map = new HashMap<String, String>();
            String base = ((EditText) layout.getChildAt(1)).getText().toString();//modifier
            String total = ((TextView)layout.getChildAt(2)).getText().toString();//total
            String id =  ((TextView) layout.getChildAt(3)).getText().toString();//id
            String name = ((TextView)layout.getChildAt(0)).getText().toString();
            map.put("name", name);
            map.put("base", base);
            map.put("total", total);
            map.put("id", id);
            fillMaps.add(map);
        }


        int toDeleteId = Integer.parseInt(((TextView) ParentRow.getChildAt(3)).getText().toString());//the id is stored in a textView inside the row. We get it's value this way.
        int tempId;
        for(int i = 0; i < fillMaps.size(); i++)
        {
            temp = fillMaps.get(i);
            tempId = Integer.parseInt(temp.get("id"));
            if(tempId == toDeleteId)
            {
                fillMaps.remove(i);
            }
        }
        adapter.notifyDataSetChanged();


    }

}
