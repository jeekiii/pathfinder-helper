package com.ucl.groupe9.pathfinderhelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PrintActivity extends Activity {

    private SQLiteDatabase db;

    ArrayList<Note> list; // initialisation of objects needed to build the ListView
    ListView notes;
    NoteAdapter adapter;

    private long toSuppress; //real index of the view to suppress

    static final int CREATE_NOTE = 1; // request codes for the startActivityForResult method
    static final int UPDATE_NOTE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Layout and activity initialisation
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);

        notes = (ListView) findViewById(R.id.notes);
        NoteSQLiteHelper nt = new NoteSQLiteHelper(this);
        db = nt.getWritableDatabase();

        Intent intent = getIntent();
        String note = intent.getStringExtra(NoteActivity.NOTE); //get the content of the intent
        String name = intent.getStringExtra(NoteActivity.NAME);

        if(note!=null) { // copies the values into the database
            ContentValues cv = new ContentValues();
            cv.put("Name", name);
            cv.put("Note", note);
            db.execSQL("INSERT INTO Notes (Name, Note) VALUES ('" + name + "','" + note + "')");
        }

        Cursor c = db.rawQuery("SELECT * FROM Notes", new String[]{});

        list = new ArrayList<>();
        for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            list.add(new Note(c.getString(0), c.getString(1), c.getString(2)));
        }

        adapter = new NoteAdapter(this.getApplicationContext(), list);
        notes.setAdapter(adapter);

        notes.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, final int position, final long viewid) { //method to change behaviour while clicked

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PrintActivity.this);

                alertDialogBuilder.setTitle("Options"); //set title

                alertDialogBuilder //set dialog message
                        .setMessage("Would you like to modify this Note?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(PrintActivity.this, NoteActivity.class);
                                Note n = (Note) notes.getItemAtPosition(position);
                                String s1 = n.getNote();
                                String s2 = n.getName();
                                intent.putExtra(NoteActivity.NOTE, s1);
                                intent.putExtra(NoteActivity.NAME, s2);
                                toSuppress = viewid;
                                startActivityForResult(intent, UPDATE_NOTE);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create(); // create alert dialog

                alertDialog.show(); // show it
            }

        /* Code pour afficher un Toast qd on touche un element
        Object o = notes.getItemAtPosition(position);
        Note fullObject = (Note) o;
        Toast.makeText(getApplicationContext(), "You have chosen: " + " " + fullObject.getName(), Toast.LENGTH_LONG).show();*/
        });



        notes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> a, View v, int position, final long viewid) { //method to change behaviour while clicked for a while

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PrintActivity.this);

                alertDialogBuilder.setTitle("Options");

                alertDialogBuilder
                        .setMessage("Do you want to delete this note? You will never be able to get it back !")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //special request to select the view to suppress in the listview
                                removeItem(viewid);
                                updateListview(list, adapter);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create(); // create alert dialog

                alertDialog.show(); // show it

                return true;
            }
        });

    c.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    //This function modifies the dataset and notify the adapter so the listview is updated

    public void updateListview(ArrayList<Note> list, NoteAdapter adapter) {
        list.clear();
        Cursor curs = db.rawQuery("SELECT * FROM Notes", new String[]{});
        for (curs.moveToFirst(); !curs.isAfterLast(); curs.moveToNext()) {
            list.add(new Note(curs.getString(0), curs.getString(1), curs.getString(2)));
        }

        //notify the changes to the adapter and close cursor
        adapter.notifyDataSetChanged();
        curs.close();
    }

    private class NoteAdapter extends BaseAdapter {

        private ArrayList<Note> array;
        private LayoutInflater mInflater;

        public NoteAdapter(Context context, ArrayList<Note> array) {
            this.array=array;
            this.mInflater=LayoutInflater.from(context);
        }

        public int getCount() {
            return array.size();
        }

        public Object getItem(int position) {
            return array.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if(convertView==null) {
                convertView = mInflater.inflate(R.layout.noteitem, null);
                holder = new ViewHolder();
                holder.txtname = (TextView) convertView.findViewById(R.id.Name);
                holder.txtnote = (TextView) convertView.findViewById(R.id.Note);

                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.txtname.setText(array.get(position).getName());
            holder.txtnote.setText(array.get(position).getNote());

            return convertView;
        }

        class ViewHolder {
            TextView txtname;
            TextView txtnote;
        }
    }

    public void onClickNewNote(View v)
    {
        Intent intent = new Intent(PrintActivity.this, NoteActivity.class);
        String s1 = "";
        String s2 = "";
        intent.putExtra(NoteActivity.NOTE, s1);
        intent.putExtra(NoteActivity.NAME, s2);
        startActivityForResult(intent, CREATE_NOTE);
    }

    public void removeItem(long indice) {
        Integer i = (int) indice+1;
        db.execSQL("DELETE FROM Notes WHERE (SELECT count(*) FROM Notes AS f WHERE f.ID <= Notes.id) == " + i + ";");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode!=RESULT_CANCELED) {

            String note = data.getStringExtra(NoteActivity.NOTE); //note
            String name = data.getStringExtra(NoteActivity.NAME); //name

            if (note != null) { // add the new note
                ContentValues cv = new ContentValues();
                cv.put("Name", name);
                cv.put("Note", note);
                db.execSQL("INSERT INTO Notes (Name, Note) VALUES ('" + name + "','" + note + "')");
            }

            list = new ArrayList<>();
            adapter = new NoteAdapter(this.getApplicationContext(), list);
            notes.setAdapter(adapter);

            if (requestCode == UPDATE_NOTE && resultCode == RESULT_OK) { // if it is a modification, we must suppress the old view first
                removeItem(toSuppress);
            }

            updateListview(list, adapter); //and then update the list and notify it to the adapter

        }

    }
    //db should never be closer otherwise exceptions occurs saying :"Attempt to re-open an already opened database"
}