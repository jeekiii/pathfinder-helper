package com.ucl.groupe9.pathfinderhelper.static_databases;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ucl.groupe9.pathfinderhelper.R;
import com.ucl.groupe9.pathfinderhelper.model.Spell;

import java.util.ArrayList;


public class Db_Spells_Adapter extends ArrayAdapter<Spell> implements Filterable {

    Context mContext;
    private ArrayList<Spell> originalData = null;
    private ArrayList<Spell>filteredData = null;
    private ItemFilter mFilter;

    public Db_Spells_Adapter(Context context, ArrayList<Spell> data) {
        super(context, R.layout.column_row_allspells,data);
        this.originalData = new ArrayList<Spell>(data) ;
        this.filteredData = new ArrayList<Spell>(data) ;
        mFilter = new ItemFilter();
        mContext = context;
    }
    @Override
    public void clear() {
        this.filteredData = new ArrayList<Spell>(originalData);
    }

    public void add(Spell m) {filteredData.add(m);}

    public int getFullCount() {
        return originalData.size();
    }

    public Spell getOriginal(int position) {
        return originalData.get(position);
    }

    public int getCount() {
        return filteredData.size();
    }

    public Spell getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if(v == null)
        {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.column_row_allspells,parent,false);
        }

        Spell m = filteredData.get(position);
        if (m != null) {

            TextView name = (TextView) v.findViewById(R.id.Name);
            TextView school = (TextView) v.findViewById(R.id.School);
            //TextView descriptor = (TextView) v.findViewById(R.id.Descriptor);

            name.setText(m.getName());
            school.setText(m.getSchool());
            //descriptor.setText(m.getDescriptor());
        }

        return v;
    }

    @Override
    public Filter getFilter() {
        if( mFilter == null) {
            mFilter = new ItemFilter();
            return mFilter;
        } else {
            return mFilter;
        }
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            String prefix = constraint.toString().toLowerCase();

            if (prefix == null || prefix.length() == 0){
                ArrayList<Spell> list = new ArrayList<Spell>(originalData);
                results.values = list;
                results.count = list.size();
            } else {
                final ArrayList<Spell> list = new ArrayList<Spell>(originalData);
                final ArrayList<Spell> nlist = new ArrayList<>();
                int count = list.size();

                for (int i = 0; i<count; i++){
                    final Spell m = list.get(i);
                    final String value = m.getName().toLowerCase();

                    if(value.contains(prefix)){
                        nlist.add(m);
                    }
                    results.values = nlist;
                    results.count = nlist.size();
                }
            }

            Log.w("DEBUG","FOUND: results: "+results.count);

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Spell>) results.values;
            notifyDataSetChanged();
        }

    }
}
