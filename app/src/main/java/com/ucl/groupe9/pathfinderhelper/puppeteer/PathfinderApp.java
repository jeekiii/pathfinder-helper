package com.ucl.groupe9.pathfinderhelper.puppeteer;

import android.app.Application;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Created by fair on 3/21/16.
 *
 * This class is Zeus. Or rather, it is the puppeteer. It controls everything without you knowing it. Or not.
 * It's a general class of the application, having many uses to make our lives easier.
 *
 * This class allows to easily obtain the context of the application, which is particularly
 * useful in MySQLiteHelper to communicate with the database.
 *
 * It's also used for easy notifications in one line of code thanks to the notify functions.
 *
 * Other 'syntactic sugar' as we may call these may be added later on.
 *
 * @author Based on Damien Mercier's model, modified by Peter Frenyo.
 * @version 1
 * @note To allow this class to be used by Android, you have to declare the following
 * AndroidManifest.xml : <application android:name="com.ucl.groupe9.pathfinderhelper.PathfinderApp"
 * (...)>(...)</application>
 */
public class PathfinderApp extends Application {
    /**
     * Reference to the application context
     */
    private static PathfinderApp context;

    /**
     * Provides the context of the application
     *
     * @return Context of the application
     */
    public static PathfinderApp getContext() {return context;}

    public void onCreate() {
        super.onCreate();
        context = (PathfinderApp) getApplicationContext();
    }

    /**
     * Displays a notification for a short duration to the user.
     *
     * @param resId Id of the ressource (R.string.* ) containing the message to be displayed.
     *
     * @see PathfinderApp#notify
     */
    public static void notifyShort(int resId) {notify(resId, Toast.LENGTH_SHORT); }

    /**
     * Displays a notification for a long duration to the user.
     *
     * @param resId Id of the ressource (R.string.* ) containing the message to be displayed.
     *
     * @see PathfinderApp#notify
     */
    public static void notifyLong(int resId) { notify(resId, Toast.LENGTH_LONG);}

    /**
     * Displays a notification to the user. This notification is centered on the screen
     * for it to be visible even when the keyboard is active.
     *
     * @param resId     Id of the ressource (R.string.*) containing the message to be displayed.
     * @param duration  Duration of display (Toast.LENGTH_SHORT or Toast.LENGTH_LONG)
     */
    private static void notify(int resId, int duration) {
        Toast msg = Toast.makeText(getContext(), getContext().getString(resId), duration);
        msg.setGravity(Gravity.CENTER, 0, 0);
        msg.show();
    }

}