package com.ucl.groupe9.pathfinderhelper;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;


public class MultipleDice extends AppCompatActivity {

    private DiceAdapter diceAdapter;
    private SpecialDiceAdapter specialAdapter;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;

    private boolean shaken = false;

    private Toast toast = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_multiple_dice);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // gridView Initialisation
        final GridView gridview = (GridView) findViewById(R.id.gridViewDice);
        this.diceAdapter = new DiceAdapter(this,null);
        gridview.setAdapter(diceAdapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                diceAdapter.remove(position);
                return;
            }
        });

        // Set ColumnNums depending on screen size
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float scalefactor = getResources().getDisplayMetrics().density * 100;
        int number = metrics.widthPixels;
        int columns = (int) ((float) number / (float) scalefactor);
        gridview.setNumColumns(columns);

        // hide some buttons
        final ImageView removebutton = (ImageView) findViewById(R.id.removeDice);
        removebutton.setVisibility(View.INVISIBLE);
        final ImageView rollbutton = (ImageView) findViewById(R.id.rollDice);
        rollbutton.setVisibility(View.INVISIBLE);


        // Make gridview for special dice
        final GridView diceListView = (GridView) findViewById(R.id.diceList);
        this.specialAdapter = new SpecialDiceAdapter(this);
        diceListView.setAdapter(this.specialAdapter);
        diceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                addDie(specialAdapter.getItem(position).getMaxValue());
                return ;
            }
        });

        // ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {


            @Override
            public void onShake(int count) {
				/*
				 * The following method, "handleShakeEvent(count):" is a stub //
				 * method you would use to setup whatever you want done once the
				 * device has been shook.
				 */
                handleShakeEvent(count);
            }
        });
        //toolbar.setTitle("Dynamic changed title");

        specialAdapter.parentwidth = number;
    }

    private void handleShakeEvent(int count) {
        if (count < 13 && diceAdapter.getCount() > 0) {
            if (this.toast == null) {
                this.toast = Toast.makeText(getApplicationContext(), "Shake the device more!", Toast.LENGTH_LONG);
                toast.show();
            } else {
                this.toast.setText("Almost there, "+(13-count) +"times more!");
            }
        } else {
            if (diceAdapter.getCount() > 0 && shaken != true) {
                diceAdapter.rollDice();
                this.shaken = true;
                this.toast = null;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Add the following line to register the Session Manager Listener onResume
        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }

    /*
        Add a die in the dice_list
     */
    private void addDie(int max_die_value) {
        diceAdapter.add(max_die_value);
        final ImageView removebutton = (ImageView) findViewById(R.id.removeDice);
        final ImageView rollbutton = (ImageView) findViewById(R.id.rollDice);
        if (removebutton.getVisibility() != View.VISIBLE) {
            removebutton.setVisibility(View.VISIBLE);
        }
        if (rollbutton.getVisibility() != View.VISIBLE) {
            rollbutton.setVisibility(View.VISIBLE);
        }
        if (shaken) {
            reset();
        }
        return;
    }
    /*
        Remove all elements of diceAdapter. Remove all the current displayed dice
     */
    public void removeAll(View v) {
        this.diceAdapter.clear();
        hideRollerButton();
        final ImageView removebutton = (ImageView) findViewById(R.id.removeDice);
        removebutton.setVisibility(View.INVISIBLE);
        unhideRollerButton();
        this.shaken = false;
        return;
    }

    /*
        Roll the Dice
     */
    public void rollDice(View v) {
        final ImageView removebutton = (ImageView) findViewById(R.id.removeDice);
        hideRollerButton();
        removebutton.setVisibility(View.VISIBLE);
        if (!shaken) {
            diceAdapter.rollDice();
            this.shaken = true;
        }
    }

    public void hideRollerButton(){
        final ImageView rollbutton = (ImageView) findViewById(R.id.rollDice);
        rollbutton.setVisibility(View.INVISIBLE);
    }

    public void unhideRollerButton(){
        final ImageView rollbutton = (ImageView) findViewById(R.id.rollDice);
        rollbutton.setVisibility(View.INVISIBLE);
    }

    /*
        Reset the current displayed dice
     */
    public void reset() {
        unhideRollerButton();
        this.diceAdapter.reset();
        this.shaken = false;
        return;
    }
    public void resetDice(View v) {
        reset();
    }

}
