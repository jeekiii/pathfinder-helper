package com.ucl.groupe9.pathfinderhelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class CharacterSheet extends Activity {

    private SimpleAdapter adapter;
    private SimpleAdapter adapter2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_sheet);

        //first listview

        Intent intent = getIntent();
        int[] abilities = new int[6];
        abilities[0] = intent.getIntExtra("Str", 10);
        abilities[1] = intent.getIntExtra("Dex", 10);
        abilities[2] = intent.getIntExtra("Cons", 10);
        abilities[3] = intent.getIntExtra("Int", 10);
        abilities[4] = intent.getIntExtra("Wis", 10);
        abilities[5] = intent.getIntExtra("Cha", 10);

        final ListView listView = (ListView) findViewById(R.id.ability);

        // shit to add
        String[] from = new String[] {"ability_name", "ability_score", "ability_modifier", "temp_adjustment", "temp_modifier"};
        int[] to = new int[] {R.id.ability_name, R.id.ability_score, R.id.ability_modifier, R.id.temp_adjustment, R.id.temp_modifier};
        ArrayList<HashMap<String, String>> alist = new ArrayList<>();

        String[] titles = new String[] {"Str", "Dex", "Cons", "Int", "Wis", "Cha"};
        String[] mod = new String[3];
        for (int a=0; a<titles.length; a++) {
            HashMap<String, String> map = new HashMap<>();

            map.put(from[0], titles[a]);
            map.put(from[1], ((Integer) abilities[a]).toString());
            int modif = (abilities[a]-10)/2;
            map.put(from[2], ((Integer) modif).toString());
            map.put(from[3], "0");
            map.put(from[4], "0");
            alist.add(map);
            switch(a) {
                case(1):
                    mod[1]=((Integer) modif).toString();
                    break;
                case(2):
                    mod[0]=((Integer) modif).toString();
                    break;
                case(4):
                    mod[2]=((Integer) modif).toString();
                    break;
            }
        }
        listView.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
        adapter = new SimpleAdapter(this, alist, R.layout.character_sheet_row_type_1, from, to);
        listView.setAdapter(adapter);

        //second listview

        String[] tab = new String[] {"Fortitude(Cons)", "Reflex(Dex)", "Will(Wis)"};

        final ListView listView2 = (ListView) findViewById(R.id.saving_throws);

        String[] from2 = new String[] {"saving_throws", "total", "base_save", "ability_mod", "magic_modifier", "misc_modifier", "temporary_modifier"};
        int[] to2 = new int[] {R.id.saving_throws, R.id.total, R.id.base_save, R.id.ability_mod, R.id.magic_modifier, R.id.misc_modifier, R.id.temporary_modifier};
        ArrayList<HashMap<String, String>> alist2 = new ArrayList<>();

        for (int a=0; a<tab.length; a++) {
            HashMap<String, String> map = new HashMap<>();

            map.put(from2[0], tab[a]);
            int modif = (abilities[a]-10)/2;
            map.put(from2[2], "0");
            map.put(from2[3], mod[a]);
            map.put(from2[4], "0");
            map.put(from2[5], "0");
            map.put(from2[6], "0");
            int i = 10 + Integer.parseInt(mod[a]);
            map.put(from2[1], ((Integer) i).toString());

            alist2.add(map);
        }

        adapter2 = new SimpleAdapter(this, alist2, R.layout.character_sheet_row_type_2, from2, to2);
        listView2.setAdapter(adapter2);

    }

}
