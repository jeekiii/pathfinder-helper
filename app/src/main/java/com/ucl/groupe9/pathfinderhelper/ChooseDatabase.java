package com.ucl.groupe9.pathfinderhelper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.ucl.groupe9.pathfinderhelper.static_databases.Db_Bestiary;
import com.ucl.groupe9.pathfinderhelper.static_databases.Db_Feats;
import com.ucl.groupe9.pathfinderhelper.static_databases.Db_MagicItems;
import com.ucl.groupe9.pathfinderhelper.static_databases.Db_Spells;

public class ChooseDatabase extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_database);

    }

    /*
    * Launches the activity allowing us to choose the databases we want to consult
    */
    public void dbBestiaryLaunch(View v) {
        Intent intent = new Intent(ChooseDatabase.this, Db_Bestiary.class);
        startActivity(intent);
    }

    public void dbFeatsLaunch(View v) {
        Intent intent = new Intent(ChooseDatabase.this, Db_Feats.class);
        startActivity(intent);
    }

    public void dbMagicItemsLaunch(View v) {
        Intent intent = new Intent(ChooseDatabase.this, Db_MagicItems.class);
        startActivity(intent);
    }

    public void dbSpellsLaunch(View v) {
        Intent intent = new Intent(ChooseDatabase.this, Db_Spells.class);
        startActivity(intent);
    }
}

