package com.ucl.groupe9.pathfinderhelper;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 */

public class Randomiser {

    final private int normal_die = 6;

    /*
        @return: a random value of a thrown die between 1 and max_die_value (both included)
     */
    public int get_die_value(int max_die_value) {
        Random random = new Random();
        int random_number = random.nextInt(max_die_value)+1;
        return (random_number);
    }

    /*
        @return: a random value of a thrown die between 1 and 6 (both included)
     */
    public int get_normal_die_value() {
        return get_die_value(normal_die);
    }

    /* Sets a random value to each die in dice_list bewteen 1 and maxValue of the die*/
    public void get_multiple_die_value(int number_dice, ArrayList<Die> dice_list) {
        if (dice_list.size() < number_dice) {
            throw new ArrayIndexOutOfBoundsException();
        }
        for (int i=0;i<number_dice;i++) {
            if (dice_list.get(i).getMaxValue() > 0) {
                dice_list.get(i).setValue(get_die_value(dice_list.get(i).getMaxValue()));
            }
        }
        return;
    }

    /*
        @return: a list of values obtainted for rolling dice. result[i] is the value
                    of the i-th die between 1 and 6 (both included)
    */
    public int[] get_multiple_normal_die_value(int number_dice) {
        int[] random_array = new int[number_dice];
        for (int i=0;i<number_dice;i++) {
                random_array[i] = get_normal_die_value();
        }
        return random_array;
    }
}
