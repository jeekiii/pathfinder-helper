package com.ucl.groupe9.pathfinderhelper.static_databases;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.ucl.groupe9.pathfinderhelper.R;
import com.ucl.groupe9.pathfinderhelper.model.Spell;
import com.ucl.groupe9.pathfinderhelper.puppeteer.SpellSQLiteHelper;

import java.util.ArrayList;

public class Db_Spells extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private ArrayList<Spell> tmp_originalData;
    private ListView List;
    private SearchView searchBar;
    private SpellSQLiteHelper Db = null;

    private Db_Spells_Adapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db_spells);

        //List = (ListView) findViewById(R.id.listView);
        Thread t = new Thread(new Runnable() {
            public void run()
            {
                tmp_originalData = Spell.get();
            }
        });
        t.start();


        //Let's store the names of all spells
        searchBar = (SearchView) findViewById(R.id.Search);
        List = (ListView) findViewById(R.id.listView);


        try {
            t.join();
            this.adapter = new Db_Spells_Adapter(this,tmp_originalData);
        } catch (InterruptedException e) {
            Log.w("DEBUG","Error on thread");
        }

       searchBar.setOnCloseListener(new SearchView.OnCloseListener(){
            @Override
            public boolean onClose() {
                adapter.getFilter().filter("");
                Log.w("DEBUG","CLOSE");
                return true;
            }
        });


        List.setTextFilterEnabled(true);
        setupSearchView();
        List.setAdapter(adapter);

    }
    private void setupSearchView() {
        searchBar.setIconifiedByDefault(false);
        searchBar.setOnQueryTextListener(this);
        searchBar.setSubmitButtonEnabled(false);
        searchBar.setQueryHint(getString(R.string.db_spells_hint));
    }

    public boolean onQueryTextChange(String newText) {
        if (newText.equals("")) {
            adapter.getFilter().filter("");
            Log.w("DEBUG","CLEAR");
        }
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        adapter.getFilter().filter(query);
        return true;
    }

    public void rowClickSpells(View v) {
        LinearLayout linearL = (LinearLayout) v;
        TextView tv = (TextView) linearL.getChildAt(0);
        String name = tv.getText().toString();
        //get description
        String description = "no description found";

        for (int i = 0; i < adapter.getFullCount(); i++) {
            if (adapter.getOriginal(i).getName().compareTo(name) == 0) {
                description = adapter.getOriginal(i).getFullText();
            }
        }
        Intent intent = new Intent(this, Db_Spells_Details.class);
        intent.putExtra("description", description);
        startActivity(intent);
    }
}
