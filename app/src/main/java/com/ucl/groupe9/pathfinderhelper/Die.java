package com.ucl.groupe9.pathfinderhelper;
import android.graphics.Color;
/**
 * Created on 23/03/16.
 */
public class Die {

    // [4 6 8 12 20 100]
    private int max_value;
    private int value;
    private int color;


    public Die(int max_value,int value) {
        this.max_value = max_value;
        this.value = value;
        setColor();
    }

    public Die(int max_value) {
        this.max_value = max_value;
        setColor();
    }

    public int getValue() {
        return this.value;
    }

    public int getMaxValue() {
        return this.max_value;
    }

    public void setMax_value(int value) {
            this.max_value = value;
    }

    public boolean setValue(int value) {
        if (value > 0 && value <= max_value) {
            this.value = value;
        } else {
            return false;
        }
        return true;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int colornb) {
        this.color = colornb;
    }

    private void setColor() {
        switch (this.max_value) {
            case 4:
                this.color = Color.parseColor("green");
                break;
            case 6:
                this.color = Color.parseColor("black");
                break;
            case 8:
                this.color = Color.parseColor("cyan");
                break;
            case 12:
                this.color = Color.parseColor("lightgrey");
                break;
            case 20:
                this.color = Color.parseColor("#FFBC00");
                break;
            case 100:
                this.color = Color.parseColor("red");
                break;
            default:
                this.color = Color.parseColor("blue");
                break;
        }
    }

    public int compareTo(Die die) {
        return this.max_value - die.getMaxValue();
    }
}
