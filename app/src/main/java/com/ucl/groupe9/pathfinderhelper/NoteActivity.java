package com.ucl.groupe9.pathfinderhelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class NoteActivity extends Activity {

    public final static String NOTE = "Note enregistrée";
    public final static String NAME = "Nom de la note";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        Intent intent = getIntent();
        String sname = intent.getStringExtra(NAME);
        String snote = intent.getStringExtra(NOTE);
        if(!sname.equals("") && !snote.equals("")) {
            EditText title = (EditText) findViewById(R.id.NoteName);
            EditText note = (EditText) findViewById(R.id.EditNote);
            title.setText(sname);
            note.setText(snote);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    public void onClickSave(View w) {

        Intent intent = new Intent(NoteActivity.this, PrintActivity.class);
        EditText edit = (EditText) findViewById(R.id.EditNote);
        String message = edit.getText().toString();
        EditText edit2 = (EditText) findViewById(R.id.NoteName);
        String name = edit2.getText().toString();

        if(!(message.matches(""))) {
            intent.putExtra(NOTE, message);
            intent.putExtra(NAME, name);
            setResult(RESULT_OK, intent);
            this.finish();
        }
        else{
            Context context = getApplicationContext();
            CharSequence text = "You should write something in the notepad to save it!";
            int duration = Toast.LENGTH_SHORT;
            Toast t = Toast.makeText(context, text, duration);
            t.show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        this.finish();
    }
}