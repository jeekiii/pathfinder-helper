package com.ucl.groupe9.pathfinderhelper;


public class Note {

    private String note;
    private String name;
    private String id;

    public Note(String id, String name, String note) {
        this.note = note;
        this.name = name;
        this.id = id;
    }

    public void setName(String name) { this.name = name; }

    public void setNote(String note) {
        this.note = note;
    }

    public String getName() {
        return this.name;
    }

    public String getNote() {
        return this.note;
    }

    public String getId() { return this.id; }
}
