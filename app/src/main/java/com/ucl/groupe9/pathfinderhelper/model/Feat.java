package com.ucl.groupe9.pathfinderhelper.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.SparseArray;

import com.ucl.groupe9.pathfinderhelper.puppeteer.FeatSQLiteHelper;

import java.util.ArrayList;

/**
 * Created by fair on 3/9/16.
 */
public class Feat {

    /*
     *
     * /!\ Important note for those reading this source code :
     * "Mob" is a common diminutive for the word "Monster" in video games.
     * We thus interchange both words a lot in this code, as they are synonyms.
     *
     */



    /*
    Some useful column names in the database
     */
    public static final String DB_TABLE = "feats";
    public static final String DB_COL_Name = "name";
    public static final String DB_COL_type = "type";
    public static final String DB_COL_description = "description";
    public static final String DB_COL_prerequisite_feats = "prerequisite_feats";
    public static final String DB_COL_id = "id";
    public static final String DB_COL_FullText = "FullText";

    /*
    Just to be safe, we use the notation tableName.columnName
     */
    public static final String DB_COL_ID = DB_TABLE + "." + DB_COL_id;
    /*
    Name of the column we order our elements by
     */
    public static String order_by = DB_COL_Name;
    /*
    Sorting order : ASC for ascending, DESC for descending
     */
    public static String order = "ASC";

    /* Name and other useful stuff */
    private String name;
    private String type;
    private String description;
    private String prerequisite_feats;
    private final int id;
    private String FullText;


    public String getName() {
        return name;
    }

    public String gettype() {
        return type;
    }

    public String getdescription() {
        return description;
    }

    public String getprequisite_feats() {
        return prerequisite_feats;
    }

    public int getId() {
        return id;
    }

    public String getFullText() {
        return FullText;
    }



    /**
     * Constructor. Puts an element from our database into our SparseArray or HashMap..
     */
    private Feat(int Id) {

        // On enregistre l'id dans la variable d'instance.
        this.id = Id;

        // On enregistre l'instance de l'élément de collection courant dans la hashMap.
        Feat.SparseArray.put(Id, this);

        // On charge les données depuis la base de données.
        loadData();
    }


    /**
     * (Re)charge les informations depuis la base de données.
     *
     * @pre L'id de l'élément est indiqué dans this.id et l'élément existe dans la base de données.
     * @post Les informations de l'élément sont chargées dans les variables d'instance de la
     * classe.
     */
    private void loadData() {
        // Récupération de la base de données en mode "lecture".

        SQLiteDatabase db = FeatSQLiteHelper.get().getReadableDatabase();
        // Colonnes pour lesquelles il nous faut les données.
        String[] columns = new String[]{DB_COL_Name, DB_COL_type, DB_COL_description, DB_COL_prerequisite_feats, DB_COL_FullText};

        // Critères de sélection de la ligne :
        String selection = DB_COL_id + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(id)};

        // Requête SELECT à la base de données.
        Cursor c = db.query(DB_TABLE, columns, selection, selectionArgs, null, null, null);

        // Placement du curseur sur le  premier résultat (ici le seul puisque l'objet est unique).
        c.moveToFirst();


        if(c.getCount() > 0)
        {
            // Copie des données de la ligne vers les variables d'instance de l'objet courant.
            this.name = c.getString(0);
            this.type = c.getString(1);
            this.description = c.getString(2);
            this.prerequisite_feats = c.getString(3);
            this.FullText = c.getString(4);
        }


        // Fermeture du curseur.
        c.close();
        db.close();
    }


    /******************************************************************************
     * STATIC part of class
     ******************************************************************************/
    /**
     * Contains the already existing instances of the objects to avoid creating two
     * instances of the same object.
     */
    private static final SparseArray<Feat> SparseArray = new SparseArray<Feat>();

    /*
     * Creates a new element (Monster) in the database
     *
     * @param param         Way too many (102)
     *
     * @return True in case of success, false in case of failure.
     * @post Registers a new element in the database
     */
    public static boolean create(String param) {
        /* May be implemented later, may just be deleted. Would require 102 parameters. */
        return false;
    }

    /**
     * Deletes an element in the database
     *
     * @param Id        Id of the monster to be erased
     *
     * @return True in case of success, false in case of failure
     * @post Deletes element from database
     */
    public static boolean delete(String Id) {
        return false;
    }

    /**
     * Inverse l'ordre de tri actuel.
     *
     * @pre La valeur de Song.order est soit ASC soit DESC.
     * @post La valeur de Song.order a été modifiée et est soit ASC soit DESC.
     */
    public static void reverseOrder() {
        if (Feat.order.equals("ASC")) {
            Feat.order = "DESC";
        } else {
            Feat.order = "ASC";
        }
    }

    /**
     * Provides the list of all in the elements in the current user collection containing
     * searchQuery in its name.
     *
     * @param searchQuery Search query.
     *
     * @return List of elements of collection corresponding to the search query.
     */
    public static ArrayList<Feat> search(String searchQuery) {
        // Selection criteria (WHERE) : have a name corresponding to the search query
        String selection = DB_COL_Name + " LIKE ?";
        String[] selectionArgs = new String[]{"%" + searchQuery + "%"};

        // Selection criteria passed onto the submethod retrieving the elements.
        return get(selection, selectionArgs);
    }

    /*
     * Provides a list of all available monsters
     *
     * @return List of elements
     */

    public static ArrayList<Feat> get() {
        // The arguments passed to the following function are the selection criteria for the recuperation method.
        return get(null, null);
    }

    /**
     * Provides an instance of an element (Monster) in the database. If the element doesn't already
     * have an instance, a new instance is created.
     *
     * @param ciId Id of the monster
     *
     * @return An instance of the monster
     * @pre The element corresponding to the Id must exist in the database
     */
    public static Feat get(int ciId) { //ciId for Collection Item ID
        if(SparseArray==null){ return null;}
        else{
        Feat ci = Feat.SparseArray.get(ciId);
        if (ci != null) {
            return ci;
        }
        return new Feat(ciId);}

    }

    /**
     * Provides the list of all objects corresponding to the search criteria.
     *
     * This method is a submethod of getMobs and searchMobs.
     *
     * @param selection     A filter declaring which elements to return, using the format of the
     *                      SQL WHERE clause (excluding the WHERE itself). Using null will return
     *                      all the elements.
     * @param selectionArgs You can include ? in selection, which will be replaced by the values
     *                      in selectionArgs, in their order of apparition in selection.
     *                      These values will be linked as chains.
     *
     * @return List of objects. The list can be empty if no object corresponds.
     */
    public static ArrayList<Feat> get(String selection, String[] selectionArgs) {
        // Initialisation of the song list.
        ArrayList<Feat> mobs = new ArrayList<Feat>();

        // Retrieval of the SQLiteHelper to get the database.
        SQLiteDatabase db = FeatSQLiteHelper.get().getReadableDatabase();

        // Columns to be retrieved. Here we only use the id of the element, the rest will be
        // retrieved through loadData() when an instance of the element is created. (this is a development choice)
        String[] columns = new String[]{DB_COL_ID};


        //System.out.println("IN GETMOBS : COLUMNS[0] : " + columns[0]);

        // SELECT request to the database
        Cursor c = db.query(DB_TABLE, columns, selection, selectionArgs, null, null, Feat.order_by + " " + Feat.order);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            // ID of the element
            int curID = c.getInt(0);
            // The instance of the element of collection is retrieved with the get(curID) method
            // (If the instance doesn't exist yet, it is created through the get method)
            //System.out.println("ID IN GETMOBS WITH GETINT(0) : " + curID);
            Feat mob = Feat.get(curID);

            // Addition of the element of collection to the list
            mobs.add(mob);

            c.moveToNext();
        }

        // Closing of the cursor and of the database
        c.close();
        db.close();

        return mobs;
    }

}
