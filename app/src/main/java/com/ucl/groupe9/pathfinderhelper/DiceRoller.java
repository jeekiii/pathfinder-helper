package com.ucl.groupe9.pathfinderhelper;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class DiceRoller extends AppCompatActivity {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;

    private ProgressBar Pb;
    private TextView Tv; // Hint text
    private ImageView Iv; // Reset Button
    private TextView Dr; // Die result

    private Randomiser die;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dice_roller);

        // Initiate Randomiser object
        die = new Randomiser();

        // ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {


            @Override
            public void onShake(int count) {
				/*
				 * The following method, "handleShakeEvent(count):" is a stub //
				 * method you would use to setup whatever you want done once the
				 * device has been shook.
				 */
                handleShakeEvent(count);
            }
        });

        Iv = (ImageView) findViewById(R.id.resetShake);
        Iv.setVisibility(View.INVISIBLE);

        Tv = (TextView)findViewById(R.id.hint);
        Tv.setTextSize(16 * getResources().getDisplayMetrics().density);

        Dr = (TextView) findViewById(R.id.dieResult);
        Dr.setVisibility(View.INVISIBLE);

    }

    private void handleShakeEvent(int count) {
        Pb = (ProgressBar) findViewById(R.id.ShakingBar);
        if (count == 4) {

            setNumber(die.get_normal_die_value());
            Dr.setVisibility(View.VISIBLE);
            Tv.setVisibility(View.INVISIBLE);
            Pb.setVisibility(ProgressBar.INVISIBLE);
            Pb.setProgress(0);
            Iv.setVisibility(View.VISIBLE);
        }
        else {
            if (count<4) {
                Pb.setProgress(Pb.getProgress() + Pb.getMax()/4);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Add the following line to register the Session Manager Listener onResume
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }

    public void setNumber(int nb) {
        Dr.setText(nb + "");
        Dr.setVisibility(View.VISIBLE);

        Tv.setVisibility(View.INVISIBLE);

        Pb.setVisibility(ProgressBar.INVISIBLE);
        Pb.setProgress(0);

        Iv.setVisibility(View.VISIBLE);
        return;
    }

    public void resetShaker(View v) {
        mSensorManager.unregisterListener(mShakeDetector);
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
        mShakeDetector.reset();
        Pb.setProgress(0);
        Pb.setVisibility(ProgressBar.VISIBLE);
        Iv.setVisibility(View.INVISIBLE);
        Tv.setVisibility(View.VISIBLE);
        Dr.setVisibility(View.INVISIBLE);


        return;
    }

}
