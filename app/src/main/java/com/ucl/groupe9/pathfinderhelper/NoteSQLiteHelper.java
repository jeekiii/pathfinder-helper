package com.ucl.groupe9.pathfinderhelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Classe utilitaire qui va gérer la connexion, la création et la mise à jour de la base de données.
 *
 * Cette classe va s'occuper de gérer la base de données. Elle s'occupera d'en créer une nouvelle
 * lors du premier lancement de l'application. Ensuite, en cas d'évolution de version de la base de
 * données (par exemple lors d'une amélioration de votre application), elle mettra à jour celle-ci
 * de manière adéquate.
 *
 * @author Damien Mercier
 * @version améliorée pour fabriquer un Notepad
 * @see <a href="http://d.android.com/reference/android/database/sqlite/SQLiteOpenHelper.html">SQLiteOpenHelper</a>
 */
public class NoteSQLiteHelper extends SQLiteOpenHelper {

    /**
     * Nom du fichier sql contenant les instructions de création de la base de données. Le fichier
     * doit être placé dans le dossier assets/
     */
    private static final String DATABASE_SQL_FILENAME = "Notes.sql";
    /**
     * Nom du fichier de la base de données.
     */
    private static final String DATABASE_NAME = "Notes.sqlite";

    /**
     * Version de la base de données (à incrémenter en cas de modification de celle-ci afin que la
     * méthode onUpgrade soit appelée).
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * Instance de notre classe afin de pouvoir y accéder facilement depuis n'importe quel objet.
     */
    private static NoteSQLiteHelper instance;

    /**
     * Constructeur. Instancie l'utilitaire de gestion de la base de données.
     * @param context Contexte de l'application.
     */
    public NoteSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        instance = this;
    }

    /**
     * Fournit une instance de notre NoteSQLiteHelper.
     * @return MySQLiteHelper
     */
    public static NoteSQLiteHelper get() {
        if (instance == null) {
            return new NoteSQLiteHelper(NoteDB.getContext());
        }
        return instance;
    }

    /**
     * Méthode d'initialisation appelée lors de la création de la base de données.
     * @param db Base de données à initialiser
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        initDatabase(db);
    }

    /**
     * Méthode de mise à jour lors du changement de version de la base de données.
     *
     * @param db         Base de données à mettre à jour.
     * @param oldVersion Numéro de l'ancienne version.
     * @param newVersion Numéro de la nouvelle version.
     *
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /**
         * @note : Ici on se contente juste de supprimer toutes les données et de les re-créer par
         * après. Dans une vraie application en production (par ex. sur le Play Store), il faudra
         * faire en sorte que les données enregistrées par l'utilisateur ne soient pas complètement
         * effacées lorsqu'on veut mettre à jour la structure de la base de données.
         */
        deleteDatabase(db);
        onCreate(db);
    }

    /**
     * Crée les tables de la base de données et les remplit.
     *
     * @param db Base de données à initialiser.
     *
     */
    public void initDatabase(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS Notes");
        db.execSQL("CREATE TABLE Notes ( ID INTEGER PRIMARY KEY, Name TEXT, Note TEXT NOT NULL)");
    }


    /**
     * Supprime toutes les tables dans la base de données.
     *
     * @param db Base de données.
     *
     */
    public void deleteDatabase(SQLiteDatabase db) {
        Cursor c = db.query("sqlite_master", new String[]{"login"}, "type = 'table' AND login NOT LIKE '%sqlite_%'", null, null, null, null, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            db.execSQL("DROP TABLE IF EXISTS " + c.getString(0));
            c.moveToNext();
        }
        c.close();
    }
}