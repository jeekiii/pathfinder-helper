package com.ucl.groupe9.pathfinderhelper;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //pour ne pas afficher le titre de l'application à gauche
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
         //   return true;
        //}
        return super.onOptionsItemSelected(item);
    }

    public void onClickAbility(View v) {
        Intent intent = new Intent(MainActivity.this, AbilityScores.class);
        startActivity(intent);
    }

    public void onClickDice(View v) {
        Intent intent = new Intent(MainActivity.this, MultipleDice.class);
        startActivity(intent);
    }

    public void onClickInitiative(View v) {
        Intent intent = new Intent(MainActivity.this, InitiativeRoller.class);
        startActivity(intent);
    }

    public void onClickDBs(View v){
        Intent intent = new Intent(MainActivity.this, ChooseDatabase.class);
        startActivity(intent);
    }

    public void onClickNotes(View w) {
        Intent intent = new Intent(MainActivity.this, PrintActivity.class);
        startActivity(intent);
    }

    public void onClickShaker(View v) {
        Intent intent = new Intent(MainActivity.this, DiceRoller.class);
        startActivity(intent);
    }

    public void onClickSpellbook(View v) {
        Intent intent = new Intent(MainActivity.this, Spellbook.class);
        startActivity(intent);
    }
}
